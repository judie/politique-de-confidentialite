![logo citations de génie](./logo-citations-de-genie.png)

# Citations de génie - politique de confidentialité


## Collecte d'information

L'application Citations de génie ne collecte aucune information.

## Autorisation

Les autorisations demandées par cette dernière n'ont pas d'autre objectif que de rendre effective les fonctionnalités qu'elle propose.

## Publicité

L'application ne comporte pas de publicité.